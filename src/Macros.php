<?php

namespace Aitom\Aitomic;

use \Latte;

class Macros extends Latte\Macros\MacroSet
{
	public static function install(Latte\Compiler $compiler)
	{
		$me = new static($compiler);

		$me->addMacro('preference', [$me, 'macroPreference']);

		$me->addMacro('patternSlot', [$me, 'macroPatternSlot']);

		$me->addMacro('aiPattern', null, null, [$me, 'macroAiPattern']);

		$me->addMacro('aiComponent', null, null, [$me, 'macroAiComponent']);

		$me->addMacro('snippet', null, null, [$me, 'macroSnipet']);

		$me->addMacro('form', [$me, 'macroForm'], [$me, 'macroFormEnd']);

		$me->addMacro('name', null, null, [$me, 'macroName']);

		$me->addMacro('link', [$me, 'macroLink']);

		$me->addMacro('plink', [$me, 'macroPlink']);

		$me->addMacro('pageLink', [$me, 'macroPageLink']);

		$me->addMacro('thumbnail', [$me, 'macroThumbnail']);

		return $me;
	}

	public function macroPreference(Latte\MacroNode $node, Latte\PhpWriter $writer)
	{
		$params = explode(', ', $node->args);

		if (isset($params[0])) {
			return $writer->write('echo $' . str_replace('.', '->', $params[0]));
		}
	}

	public function macroPatternSlot(Latte\MacroNode $node, Latte\PhpWriter $writer)
	{
		return;
	}

	public function macroAiPattern(Latte\MacroNode $node, Latte\PhpWriter $writer)
	{
		return;
	}

	public function macroAiComponent(Latte\MacroNode $node, Latte\PhpWriter $writer)
	{
		return;
	}

	public function macroSnipet(Latte\MacroNode $node, Latte\PhpWriter $writer)
	{
		return 'echo \' id="snippet-pc-' . rand(10, 100) . '-content"\'';
	}

	public function macroForm(Latte\MacroNode $node, Latte\PhpWriter $writer)
	{
		$method = 'POST';

		if (!count($node->tokenizer->fetchWords())) {
			return;
		}

		$name = $node->tokenizer->fetchWords()[0];
		$params = explode(', ', substr($writer->formatArray(), 1, -1));
		$attr = [];

		if (!empty($params)) {
			foreach ($params as $param) {
				$param = str_replace("'", "", $param);
				$param = explode('=>', $param);
				if (isset($param[0]) && isset($param[1])) {
					$attr[] = trim($param[0]) . '="' . trim($param[1]) . '"';
				}
			}
		}

		if ($name == 'searchForm') {
			$method = 'GET';
		}

		return 'echo \'<form action="." method="' . $method . '" id="frm-' . $name . '"' . (!empty($attr) ? ' ' . implode(' ', $attr) : '') . '>\'';
	}

	public function macroFormEnd(Latte\MacroNode $node, Latte\PhpWriter $writer)
	{
		return 'echo \'</form>\'';
	}

	public function macroName(Latte\MacroNode $node, Latte\PhpWriter $writer)
	{
		if (isset($node->htmlNode->attrs['name'])) {
			throw new CompileException('It is not possible to combine name with n:name.');
		}

		$names = $node->tokenizer->fetchWords();

		if (empty($names)) {
			return;
		}

		$tag = strtolower($node->htmlNode->name);
		$name = '"' . $names[0] . '"' . (count($names) >= 2 && isset($node->htmlNode->attrs['type']) && $node->htmlNode->attrs['type'] != 'radio' ? '."[]"' :  '');
		$id = '"frm-pc-form-"."' . implode('"."-"."', $names) . '"';

		$node->isEmpty = $tag === 'input';

		if ($tag === 'label') {
			return $writer->write('echo \' for="\', %escape(' . $id . '), \'"\'');
		} elseif ($tag === 'form') {
			return;
		} else {
			return $writer->write('echo \' name="\', %escape(' . $name . '), \'" id="\', %escape(' . $id . '), \'"\'');
		}
	}

	public function macroLink(Latte\MacroNode $node, Latte\PhpWriter $writer)
	{
		return 'echo \'#\';';
	}

	public function macroPlink(Latte\MacroNode $node, Latte\PhpWriter $writer)
	{
		return 'echo \'#\';';
	}

	public function macroPageLink(Latte\MacroNode $node, Latte\PhpWriter $writer)
	{
		$params = explode(', ', $node->args);

		if (isset($params[1])) {
			return $writer->write('echo \'./\',' . $params[1]);
		}
	}

	public function macroThumbnail(Latte\MacroNode $node, Latte\PhpWriter $writer)
	{
		$params = explode(', ', $node->args);

		if (isset($params[1])) {
			$size = explode('x', $params[1]);
			$width = $size[0] ?? '';
			$height = $size[1] ?? '';
			if ($width == '*') {
				$width = $height;
			}
			if ($height == '*') {
				$height = $width;
			}
			return "echo 'data:image/svg+xml;charset=utf-8,%3Csvg xmlns%3D\'http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg\' style%3D\'background:%23ededed\' viewBox%3D\'0 0 $width $height\'%2F%3E'";
		}
	}
}
