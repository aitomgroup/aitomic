{contentType application/javascript}

var Aitomic = (function() {
    var $dialog = {
        wrap: $('.mdl-js-dialog'),
        title: $('.mdl-dialog__title', '.mdl-js-dialog'),
        content: $('.mdl-dialog__content', '.mdl-js-dialog'),
        close: $('.mdl-button.close', '.mdl-js-dialog')
    };

    var $flashMessage = $('#mdl-flash-messages');

    var loadScript = function(url, callback) {
        var getType = function(fileName) {
            var parse = fileName.split(".");
            var suffix = parse[parse.length - 1];
            return suffix;
        }

        var type = getType(url);

        if (type == 'css') {
            callback = function() {
                $('<link rel="stylesheet" href="'+url+'" />').appendTo('head');
            };
        }

        return $.get(url, undefined, callback, (type == 'css' ?  'text' : 'script'));
    };

    var codeBeautify = function($code) {
        var $beautify = $code.parent('pre').prev('textarea.jq_beautify');

        if (typeof(html_beautify) != 'undefined' && $beautify.length) {
            var $value = $beautify.val();

            $value = $value.replace(/<style>(.|\n|\r)+<\/style>/gm, '');
            $value = $value.replace(/<script>(.|\n|\r)+<\/script>/gm, '');

            $code.text(
                html_beautify($value, { 
                    preserve_newlines: false
                })
            );
        }
    };

    var codeHighlight = function($code) {
        codeBeautify($code);

        $code.addClass('language-' + $code.data('language'));

        Prism.highlightElement(
            $code[0]
        );

        codeCopy($code);
    };

    var codeCopy = function($code) {
        if (typeof(ClipboardJS) != 'undefined') {
            var $copy = $('<span style="background: #e4e1e0; position: fixed; right: 40px; bottom: 40px" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect"><i class="material-icons">file_copy</i> Copy</span>');

            $code.parent('pre').after($copy);

            new ClipboardJS($copy[0], {
                target: function() {
                    return $code[0];
                }
            }).on('success', successCopy);
        }
    };

    var successCopy = function() {
        if ($flashMessage.length) {
            $flashMessage[0].MaterialSnackbar.showSnackbar({
                message: 'Copied to clipboard!',
                timeout: 2000
            });
        }
    };

    var openDescription = function() {
        var context = {
            title: $(this).data('title') || '',
            package: $(this).data('package') || '',
            version: $(this).data('version') || '',
            readme: $(this).data('readme') || '',
            dependencies: $(this).data('dependencies') || '',
            description: ''
        };

        if (context.package != '' && context.version != '') {
            context.description += '<p>Version: ' + context.version + ' <a href="https://www.npmjs.com/package/' + context.package + '/v/' + context.version + '?activeTab=versions" target="_blank" class="mdl-color-text--primary">(Changelog)</a></p>';
        }

        if (context.package != '') {
            context.description += '<p><pre><code class="npm-command" style="background: #f5f2f0; padding: 5px">npm i ' + context.package + ' --save</code></pre></p>';
        }

        if (context.readme != '') {
            context.description += '<p>' + context.readme + '';
        }

        if (context.dependencies != '') {
            context.description += '<h5>Dependencies</h5>';
            context.description += '<table class="mdl-data-table mdl-shadow--2dp reset" style="margin-bottom: 20px; width: 100%">';
                context.description += '<thead>';
                    context.description += '<tr>';
                        context.description += '<th class="mdl-data-table__cell--non-numeric">Name</th>';
                        context.description += '<th class="mdl-data-table__cell--non-numeric">Version</th>';
                    context.description += '</tr>';
                    context.description += '<tbody>';
                    $.each(context.dependencies, function(name, version) {
                        context.description += '<tr><td class="mdl-data-table__cell--non-numeric">' + name + '</td><td class="mdl-data-table__cell--non-numeric">' + version + '</td></tr>';
                    });
                context.description += '</tbody>';
            context.description += '</table>';
        }

        if (context.package != '' && context.version != '') {
            context.description += '<p><a href="https://www.npmjs.com/package/' + context.package + '/v/' + context.version + '?activeTab=dependents" target="_blank" class="mdl-color-text--primary">Check dependents</a></p>';
        }

        $dialog.title.text(context.title);

        $dialog.content.html(context.description);

        if (typeof(ClipboardJS) != 'undefined') {
            var $code = $dialog.content.find('.npm-command');

            $code.css('cursor', 'pointer');

            $dialog.clipboard = new ClipboardJS($code[0], {
                target: function() {
                    return $code[0];
                }
            });

            $dialog.clipboard.on('success', successCopy);
        }

        $dialog.wrap[0].showModal();
    };

    var initDialog = function() {
        if (!$dialog.wrap[0].showModal) {
            $.when
             .apply($, [
                loadScript('https://cdnjs.cloudflare.com/ajax/libs/dialog-polyfill/0.4.2/dialog-polyfill.min.js'),
                loadScript('https://cdnjs.cloudflare.com/ajax/libs/dialog-polyfill/0.4.2/dialog-polyfill.min.css')
            ])
             .then(function(){
                dialogPolyfill.registerDialog($dialog.wrap[0]);
            });
        }

        $dialog.close.click(function() {
            $dialog.wrap[0].close();
        });

        $('.jq_description').click(openDescription);
    };

    var initHighlight = function() {
        var $panel = $(this.hash);

        if ($panel.find('.jq_highlight').length) {
            var $code = $panel.find('.jq_highlight');

            if (!$code.is('[class*="language-"]')) {
                var scriptsQueue = [];

                if (typeof(Prism) == 'undefined') {
                    scriptsQueue.push(loadScript('https://cdnjs.cloudflare.com/ajax/libs/prism/1.15.0/prism.min.js'));
                    scriptsQueue.push(loadScript('https://cdnjs.cloudflare.com/ajax/libs/prism/1.15.0/themes/prism.min.css'));
                }

                if (typeof(ClipboardJS) == 'undefined')
                    scriptsQueue.push(loadScript('https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/2.0.0/clipboard.min.js'));

                if (typeof(html_beautify) == 'undefined' && $panel.find('.jq_beautify'))
                    scriptsQueue.push(loadScript('https://cdnjs.cloudflare.com/ajax/libs/js-beautify/1.8.8/beautify-html.min.js'));

                if (scriptsQueue && scriptsQueue.length) {
                    $.when.apply($, scriptsQueue).then(function(){
                        codeHighlight($code);
                    });
                } else {
                    codeHighlight($code);
                }
            }
        }
    };

    var init = function() {
        if ($dialog.wrap.length) initDialog();

        $('.mdl-layout__tab-bar').on('click', '.mdl-layout__tab', initHighlight);
    };

    return {
        init: init
    };
})();

$(document).ready(Aitomic.init);