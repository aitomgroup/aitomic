<?php

namespace Aitom\Aitomic;

class Helpers
{
	public function __construct($engine)
	{
		$engine->addFilter('translate', 'sprintf');

		$engine->addFilter('makeLinks', [$this, 'helperMakeLinks']);

		$engine->addFilter('phone', [$this, 'helperPhone']);

		$engine->addFilter('preWrap', [$this, 'helperPreWrap']);

		$engine->addFilter('webalize', [$this, 'helperWebalize']);

		return $engine;
	}

	public function helperMakeLinks($content)
	{
		return $content;
	}

	public function helperPhone($input)
	{
		return preg_replace('/\s+/', '', $input);
	}

	public function helperPreWrap($text)
	{
		$prepositionList = 'k|s|v|z|the|a|i|o|u|č.|ve|ke|ku|že|na|do|od|pod|za|pro|tj\.|mj\.|tzv\.|tzn\.';

		return preg_replace("/(?<=\s|^|>)(".$prepositionList.") /i", '$1&nbsp;', $text);
	}

	public function helperWebalize($s, $charlist = null, $lower = true)
	{
		//$s = self::toAscii($s);

		if ($lower) {
			$s = strtolower($s);
		}

		$s = preg_replace('#[^a-z0-9' . preg_quote($charlist, '#') . ']+#i', '-', $s);

		$s = trim($s, '-');

		return $s;
	}
}
