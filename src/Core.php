<?php

namespace Aitom\Aitomic;

use Tracy\Debugger;
use Latte\CompileException;

class Core
{
	private $engine;

	public $params = [];

	public function __construct()
	{
		if (in_array($_SERVER['REMOTE_ADDR'], ['127.0.0.1', '::1'])) {
			Debugger::enable();
		}

		$this->engine = new \Latte\Engine;

		$this->setup();

		$this->params();
	}

	private function setup()
	{
		$this->engine->setTempDirectory('temp');

		$this->engine->onCompile[] = function () {
			Macros::install($this->engine->getCompiler());
		};

		new Helpers($this->engine);
	}

	public function params()
	{
		$this->params = [
			'content' => ''
		];
	}

	public function import($dir = null, $search = '~\.(json|latte|neon|scss|js)$~', $return = false)
	{
		if (is_null($dir)) {
			throw new CompileException('First argument require set import directory.');
		}

		$dir = rtrim($dir, '/');
		$files = preg_grep($search, scandir($dir));

		if (empty($files)) {
			throw new CompileException('No {latte} template');
		}

		$params = !$return ? $this->params : [];

		foreach ($files as $file) {
			$file = pathinfo($file);
			$extension = $file['extension'];
			$content = file_get_contents($dir . '/' . $file['basename'], true);

			if ($extension == 'json') {
				$params = array_merge(
					$params,
					(array) json_decode($content, false)
				);
			} else {
				$params[$extension] = $content;

				if ($extension == 'latte') {
					$params['dir'] = realpath($dir);
				}
			}
		}

		if (!$return) {
			$this->params = $params;
		} else {
			return $params;
		}
	}

	public function template() {
		return '@layout.latte';
	}

	public function render()
	{
		$this->params = Params::filter($this->params);

		$this->params = Params::tree($this->params);

		if (!empty($this->params['content'])) {
			$this->params['content'] = $this->renderToString($this->params['content'], $this->params);
		}

		echo $this->engine->render(__DIR__ . '/templates/' . $this->template(), $this->params);
	}

	public function renderToString($string, $params = null) {
		if (is_null($params)) {
			$params = $this->params;
		}

		$blocks = [
			'main' => $string
		];

		if ((preg_match('/nodeModules\b/', $blocks['main']) && isset($params['nodeModules'])) ||
			(preg_match('/nodeModulesPath\b/', $blocks['main']) && isset($params['nodeModulesPath']))) {
			$blocks['main'] = str_replace(['$nodeModules', '${nodeModules}', '$nodeModulesPath', '${nodeModulesPath}'], isset($params['nodeModulesPath']) ? $params['nodeModulesPath'] : $params['nodeModules'], $blocks['main']);
		}

		if (preg_match_all('/{(include|import) [\"\']([^\"\']+)/', $blocks['main'], $macros, PREG_SET_ORDER) && isset($params['dir'])) {
			foreach ($macros as $macro) {
				$type = $macro[1];
				$template = $macro[2];
				$file = $params['dir'] . '/' . $template;

				if (file_exists($file) && !isset($blocks[$template])) {
					$blocks[$template] = file_get_contents($file, true);
				}

				if ($type == 'import') {
					$blocks['#' . basename($file, '.latte')] = $blocks[$template];
				}
			}
		}

		$this->engine->setLoader(new \Latte\Loaders\StringLoader($blocks));

		$string = $this->engine->renderToString('main', $params);

		$this->engine->setLoader(new \Latte\Loaders\FileLoader);

		return $string;
	}
}
