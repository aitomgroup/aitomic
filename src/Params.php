<?php

namespace Aitom\Aitomic;

class Params
{
	private static $prefixPackage = 'aitomic-';

	public static function getPrefix()
	{
		return self::$prefixPackage;
	}

	private static $categories = [
		'b' => 'base',
		'a' => 'atoms',
		'm' => 'molecules',
		'o' => 'organism'
	];

	public static function category($abbr)
	{
		if (isset(self::$categories[$abbr])) {
			return self::$categories[$abbr];
		} elseif (in_array($abbr, self::$categories)) {
			return $abbr;
		}
	}

	public static function filter($params)
	{
		if (!isset($params['basePath'])) {
			$params['basePath'] = '../';
		}

		if (!isset($params['nodeModules']) && !isset($params['nodeModulesPath'])) {
			$params['nodeModules'] = $params['nodeModulesPath'] = '../node_modules';
		}

		if (!isset($params['nodeModulesPath']) && isset($params['nodeModules'])) {
			$params['nodeModulesPath'] = $params['nodeModules'];
		}

		if (isset($params['nodeModulesPath']) && !isset($params['nodeModules'])) {
			$params['nodeModules'] = $params['nodeModulesPath'];
		}

		if (empty($params['content']) && !empty($params['latte'])) {
			$params['content'] = $params['latte'];

			if (preg_match('/button(\d?)\|nocheck/', $params['content'])) {
				$params['content'] = preg_replace('/button(\d?)\|nocheck/', 'button$1->link|nocheck', $params['content']);
			}

			if (preg_match('/(\\$input instanceof Nette\\\\Forms\\\\Controls\\\\(\w+))/', $params['content'])) {
				$params['content'] = preg_replace('/(\\$input instanceof Nette\\\\Forms\\\\Controls\\\\(\w+))/', '$input->type == \'$2\'', $params['content']);
			}
		}

		if (!empty($params['filterContent'])) {
			if (!empty($params['content'])) {
				foreach ($params['filterContent'] as $filter) {
					if (!is_object($filter) ||
						!isset($filter->pattern) ||
						!isset($filter->replacement)) {
						continue;
					}

					$params['content'] = str_replace($filter->pattern, $filter->replacement, $params['content']);
				}
			}

			unset($params['filterContent']);
		}

		if (!isset($params['name'])) {
			$params['name'] = $params['package'] = 'component';
		} elseif (self::$prefixPackage != '' && strpos($params['name'], self::$prefixPackage) !== false) {
			$params['package'] = $params['name'];
			$params['name'] = str_replace(self::$prefixPackage, '', $params['name']);
			if (strpos($params['name'], '-') == 1) {
				$params['single'] = substr($params['name'], 0, 1) . '/' . substr($params['name'], 2);
			}
		}

		if (!isset($params['version'])) {
			$params['version'] = '0.0.1';
		}

		if (isset($params['tags'])) {
			$params['tags'] = (array) $params['tags'];
		}

		if (isset($params['md'])) {
			$md = new \Parsedown();

			$params['description'] = $md->text($params['md']);

			unset($params['md']);
		}

		if (isset($params['pageProperties']->tags)) {
			$params['pageProperties']->tags = (array) $params['pageProperties']->tags;
		}

		if (isset($params['languages'])) {
			$params['languages'] = (array) $params['languages'];
		}

		if (isset($params['photolist'])) {
			$params['settings']['enableVideo'] = true;
		}

		if (isset($params['form']->groups)) {
			$params['form'] = new class($params['form']->groups)
			{
				private $groups;

				public function __construct($groups)
				{
					$this->groups = $groups;
				}

				public function getGroups()
				{
					$return = [];

					foreach ($this->groups as $group) {
						$return[] = new class($group)
						{
							private $options;

							public function __construct($group)
							{
								$this->options = (array) $group;
							}

							public function getOption($name)
							{
								if (isset($this->options[$name])) {
									return $this->options[$name];
								}
							}

							public function getControls()
							{
								if (isset($this->options['controls'])) {
									$controls = [];

									foreach ($this->options['controls'] as $control) {
										$controls[] = new class($control)
										{
											public $options;
											public $type;
											public $name;
											public $caption;
											public $prompt;
											public $value;
											public $items;
											public $control;

											public function __construct($control)
											{
												$this->options = (array) $control;
												$this->type = $control->type;
												$this->name = $control->name;
												$this->caption = $control->caption;
												$this->prompt = isset($control->prompt) ? $control->prompt : '';
												$this->value = isset($control->value) ? $control->value : '';
												$this->items = isset($control->items) ? $control->items : '';
												$this->control->attrs['data-nette-rules'][0]['op'] = ':fileSize';
												$this->control->attrs['data-nette-rules'][0]['arg'] = 2100000;

												if (!empty($control->attrs)) {
													$this->control->attrs += array_map(
														function($attr) {
															return is_array($decode = json_decode($attr)) ? $decode : $attr;
														},
														(array) $control->attrs
													);
												}
											}

											public function isRequired()
											{
												return false;
											}
										};
									}

									return $controls;
								}
							}
						};
					}

					return $return;
				}
			};
		}

		if (isset($params['input']->control)) {
			$params['input']->control->attrs['data-nette-rules'][0]['op'] = ':fileSize';
			$params['input']->control->attrs['data-nette-rules'][0]['arg'] = 2100000;

			if (!empty($params['input']->attrs)) {
				$params['input']->control->attrs += array_map(
					function($attr) {
						return is_array($decode = json_decode($attr)) ? $decode : $attr;
					},
					(array) $params['input']->attrs
				);
			}
		}

		if (isset($params['paginator'])) {
			$params['paginator'] = new class
			{
				public function isFirst()
				{
					return true;
				}

				public function isLast()
				{
					return false;
				}
			};
			$params['paginator']->page = 1;
			$params['paginator']->pageCount = 2;
			$params['steps'] = [1, 2, 3, 10];
		}

		return $params;
	}

	public static function tree($params)
	{
		if (in_array($_SERVER['REMOTE_ADDR'], ['127.0.0.1', '::1'])) {
			return $params;
		}

		$tree = @file_get_contents('http://registry.npmjs.org/aitomic-docs/latest');

		if (!$tree) {
			return $params;
		}

		$tree = json_decode($tree, true);

		if (empty($tree['dependencies'])) {
			return $params;
		}

		foreach (self::$categories as $category) {
			$params['tree'][$category] = [];
		}

		if (isset($tree['dependencies']['aitomic-base'])) {
			$params['tree']['base'] = [
				'normalize' => 'normalize',
				'document' => 'document',
				'grid' => 'grid',
				'helpers' => 'helpers'
			];

			unset($tree['dependencies']['aitomic-base']);
		}

		foreach ($tree['dependencies'] as $package => $version) {
			if (self::$prefixPackage == '' || strpos($package, self::$prefixPackage) === false) {
				continue;
			}

			$package = str_replace(self::$prefixPackage, '', $package);

			$name = substr($package, strpos($package, '-') + 1);

			$category = self::category(substr($package, 0, 1));

			$params['tree'][$category][$name] = $package;
		}

		return $params;
	}
}
