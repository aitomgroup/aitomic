<?php

require_once('vendor/autoload.php');

require_once('src/Core.php');
require_once('src/Helpers.php');
require_once('src/Macros.php');
require_once('src/Params.php');

$aitomic = new Aitom\Aitomic\Core();

$aitomic->render();